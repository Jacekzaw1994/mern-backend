require('./express')({
    database: require('./utils/database')
}).listen(3030, () => {
    console.log('API listening on port 3030!');
});


// const express = require('express');
// const app = express();
// const bodyParser = require('body-parser');
// const mongoose = require('mongoose');
// User = require('./models/User');
// Post = require('./models/Post');
// Relation = require('./models/Relation');
// UserDetails = require('./models/UserDetails');
// UserAim = require('./models/UserAim');
// Like = require('./models/Like');
// Comment = require('./models/Comment');
//
// app.use(bodyParser.json());
//
//
// // Connect to Mongoose
// mongoose.connect('mongodb://localhost/thesis');
// var db = mongoose.connection;
//
//
// app.get("/", function (req, res) {
//     res.send("Please use /api/users | /api/posts | /api/comments | /api/relations | /api/likes | /api/useraim | /api/userdetails");
// });
//
// //********************************************************
// // ======================= USER =========================
// //********************************************************
//
// // -------- GET USERS -----------
//
// app.get("/api/users", function (req, res) {
//     User.getUsers(function (err, users) {
//         if (err) {
//             throw err;
//         }
//         res.json(users);
//     });
// });
//
// // -------- POST USER -----------
//
// app.post("/api/users", function (req, res) {
//     var user = req.body;
//     User.addUser(user, function (err, user) {
//         if (err) {
//             throw err;
//         }
//         res.json(user);
//     });
// });
//
// // -------- PUT USER (UPDATE) -----------
//
// app.put("/api/users/:_id", function (req, res) {
//     var id = req.params._id;
//     var user = req.body;
//     User.updateUser(id, user, {}, function (err, user) {
//         if (err) {
//             throw err;
//         }
//         res.json(user);
//     });
// });
//
// // -------- GET USER BY ID -------
//
// app.get("/api/users/:_id", function (req, res) {
//     User.getUserById(req.params._id, function (err, user) {
//         if (err) {
//             throw err;
//         }
//         res.json(user);
//     });
// });
//
// // --------  DELETE USER -----------
//
// app.delete("/api/users/:_id", function (req, res) {
//     var id = req.params._id;
//     User.removeUser(id, function (err, user) {
//         if (err) {
//             throw err;
//         }
//         res.json(user);
//     });
// });
//
// //********************************************************
// // ======================= POSTS =========================
// //********************************************************
//
// // -------- GET POSTS -------
//
// app.get("/api/posts", function (req, res) {
//     Post.getPosts(function (err, posts) {
//         if (err) {
//             throw err;
//         }
//         res.json(posts);
//     });
// });
//
// // -------- GET POST BY ID -------
//
// app.get("/api/posts/:_id", function (req, res) {
//     Post.getPostById(req.params._id, function (err, post) {
//         if (err) {
//             throw err;
//         }
//         res.json(post);
//     });
// });
//
// // -------- PUT POSTS (UPDATE) -----------
//
// app.put("/api/posts/:_id", function (req, res) {
//     var id = req.params._id;
//     var post = req.body;
//     Post.updatePost(id, post, {}, function (err, post) {
//         if (err) {
//             throw err;
//         }
//         res.json(post);
//     });
// });
//
// // -------- (POST) ADD POST -------
//
//
// app.post("/api/posts", function (req, res) {
//     var post = req.body;
//     Post.addPost(post, function (err, post) {
//         if (err) {
//             throw err;
//         }
//         res.json(post);
//     });
// });
//
// // --------  DELETE POST (UPDATE) -----------
//
// app.delete("/api/posts/:_id", function (req, res) {
//     var id = req.params._id;
//     Post.removePost(id, function (err, post) {
//         if (err) {
//             throw err;
//         }
//         res.json(post);
//     });
// });
//
// //********************************************************
// // ======================= RELATIONS =========================
// //********************************************************
//
// // -------- GET RELATIONS -----------
//
// app.get("/api/relations", function (req, res) {
//     Relation.getRelations(function (err, relations) {
//         if (err) {
//             throw err;
//         }
//         res.json(relations);
//     });
// });
//
// // -------- POST RELATION -----------
//
// app.post("/api/relations", function (req, res) {
//     var relation = req.body;
//     Relation.addRelation(relation, function (err, relation) {
//         if (err) {
//             throw err;
//         }
//         res.json(relation);
//     });
// });
//
// // -------- GET RELATION BY ID -------
//
// app.get("/api/relations/:_id", function (req, res) {
//     Relation.getRelationById(req.params._id, function (err, relation) {
//         if (err) {
//             throw err;
//         }
//         res.json(relation);
//     });
// });
//
// //********************************************************
// // ======================= COMMENT =========================
// //********************************************************
//
// // -------- GET COMMENTS -------
//
// app.get("/api/comments", function (req, res) {
//     Comment.getComments(function (err, comments) {
//         if (err) {
//             throw err;
//         }
//         res.json(comments);
//     });
// });
//
// // -------- GET COMMENT BY ID -------
//
// app.get("/api/comments/:_id", function (req, res) {
//     Comment.getCommentById(req.params._id, function (err, comment) {
//         if (err) {
//             throw err;
//         }
//         res.json(comment);
//     });
// });
//
// // -------- (POST) ADD COMMENT -------
//
//
// app.post("/api/comments", function (req, res) {
//     var comment = req.body;
//     Comment.addComment(comment, function (err, comment) {
//         if (err) {
//             throw err;
//         }
//         res.json(comment);
//     });
// });
//
// //********************************************************
// // ======================= USER AIM =========================
// //********************************************************
//
// // -------- GET USER AIM -------
//
// app.get("/api/useraim", function (req, res) {
//     UserAim.getUserAim(function (err, user_aim) {
//         if (err) {
//             throw err;
//         }
//         res.json(user_aim);
//     });
// });
//
// // -------- GET USER AIM BY ID -------
//
// app.get("/api/useraim/:_id", function (req, res) {
//     UserAim.getUserAimById(req.params._id, function (err, user_aim) {
//         if (err) {
//             throw err;
//         }
//         res.json(user_aim);
//     });
// });
//
// // -------- (POST) ADD USER AIM -------
//
//
// app.post("/api/useraim", function (req, res) {
//     var user_aim = req.body;
//     UserAim.addUserAim(user_aim, function (err, user_aim) {
//         if (err) {
//             throw err;
//         }
//         res.json(user_aim);
//     });
// });
//
// //********************************************************
// // ======================= USER DETAILS =====================
// //********************************************************
//
// app.get("/api/userdetails", function (req, res) {
//     UserDetails.getUserDetails(function (err, user_details) {
//         if (err) {
//             throw err;
//         }
//         res.json(user_details);
//     });
// });
//
// // -------- GET USER DETAILS BY ID -------
//
// app.get("/api/userdetails/:_id", function (req, res) {
//     UserDetails.getUserDetailById(req.params._id, function (err, user_detail) {
//         if (err) {
//             throw err;
//         }
//         res.json(user_detail);
//     });
// });
//
// // -------- (POST) ADD USER DETAILS -------
//
//
// app.post("/api/userdetails", function (req, res) {
//     var user_details = req.body;
//     UserDetails.addUserDetails(user_details, function (err, user_details) {
//         if (err) {
//             throw err;
//         }
//         res.json(user_details);
//     });
// });
//
// //********************************************************
// // ======================= LIKE =========================
// //********************************************************
//
// // ---------------- GET LIKES -------------------
//
// app.get("/api/likes", function (req, res) {
//     Like.getLikes(function (err, likes) {
//         if (err) {
//             throw err;
//         }
//         res.json(likes);
//     });
// });
//
// // -------- GET LIKE BY ID -------
//
// app.get("/api/likes/:_id", function (req, res) {
//     Like.getLikeById(req.params._id, function (err, like) {
//         if (err) {
//             throw err;
//         }
//         res.json(like);
//     });
// });
//
// // -------- (POST) ADD LIKE -------
//
//
// app.post("/api/likes", function (req, res) {
//     var like = req.body;
//     Like.addLike(like, function (err, like) {
//         if (err) {
//             throw err;
//         }
//         res.json(like);
//     });
// });
//
// // SERVER LISTEN AT DEFINED PORT
//
// app.listen(8081, function () {
//     console.log("Serwer został uruchomiony adresem http://localhost:8081")
//     ;
// });