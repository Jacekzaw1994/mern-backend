const env = require('../config/environment');

module.exports = (err, req, res, next) => {
    if (err.name === 'ValidationError') {

        res.status(400);
        return res.json({
            message: err.message,
            validation: err.errors,
            error: env === 'development' ? err : undefined
        });

    } else {

        res.status(err.status || err.statusCode || 500);
        return res.json({
            message: err.message,
            error: env === 'development' ? err : undefined
        });

    }
};