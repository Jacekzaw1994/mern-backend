const bcrypt = require('bcrypt');

exports.hash = (str) => {
    return bcrypt.hashSync(str, 10);
};

exports.compare = (str, hash) => {
    return bcrypt.compareSync(str, hash);
};