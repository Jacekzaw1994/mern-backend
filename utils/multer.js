const fs = require('fs');
const path = require('path');
const multer = require('multer');
const mime = require('mime-types');

exports.upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path.join(__dirname, '../public/uploads'));
        },
        filename: (req, file, cb) => {
            const {mimetype} = file;
            const ext = mime.extension(mimetype) || 'bin';
            cb(null, `upload-${Date.now()}.${ext}`)
        },
    }),
});

exports.remove = (filePath) => {
    filePath = path.join(__dirname, '../public', filePath);
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) return reject(err);
            return resolve();
        });
    });
};