const path = require('path');
const fs = require('fs');
const morgan = require('morgan');
const env = require('../config/environment');

module.exports = () => {
    if (env === 'development') {
        return morgan('dev');
    } else {
        const logfile = path.join(__dirname, '../api.log');
        const stream = fs.createWriteStream(logfile, {flags: 'a'});
        return morgan('common', {
            skip: (req, res) => res.statusCode < 400,
            stream,
        });
    }
};