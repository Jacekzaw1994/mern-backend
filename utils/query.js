const isNumber = require('lodash').isNumber;

exports.getSkip = ({skip}) => {
    skip = parseInt(skip, 10);
    return (isNumber(skip) && skip > 0) ? skip : 0;
};

exports.getLimit = ({limit}) => {
    limit = parseInt(limit, 10);
    return (isNumber(limit) && limit > 0) ? limit : undefined;
};
