const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const passport = require('passport');
const jwt = require('jsonwebtoken');
const get = require('lodash').get;
const error = require('./error');
const secret = require('../config/secret');

function verifyTokenPayload(User) {
    return ({ _id }, callback) => {
        User.findById(_id)
            .then(user => callback(null, user || false))
            .catch(err => callback(err, false));
    };
}

exports.authorize = ( owner = null) => {
    return (req, res, next) => {
        passport.authenticate('jwt', {
            session: false
        })(req, res, (err) => {
            if (err) return next(err);
            let allow = false;

            if (owner) {
                const userId = `${req.user._id}`;
                let reqId = get(req, owner, null);
                reqId = reqId ? `${reqId}` : null;
                allow = reqId === userId;
            }

            if (allow) {
                return next();
            } else {
                return next(error(401, 'You don\'t have permission'));
            }

        });
    };
};

exports.initialize = ({models: {User}}) => {
    passport.use(new JwtStrategy({
        jwtFromRequest: ExtractJwt.fromAuthHeader(),
        secretOrKey: secret
    }, verifyTokenPayload(User)));
    return passport.initialize();
};

exports.encodeToken = (_id) => {
    return jwt.sign({ _id }, { expiresIn: '7d' });
};