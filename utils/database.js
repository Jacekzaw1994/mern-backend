const mongoose = require('mongoose');
const database = require('../config/database');
const env = require('../config/environment');

mongoose.connect(database);
mongoose.Promise = global.Promise;

if (env === 'development') {
    mongoose.set('debug', true);
}

module.exports = mongoose;