const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Comment Schema
const schema = exports.schema = new Schema({
    content: {
        type: String,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    post: {
        type: String,
        ref: 'Post',
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
        index: true
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

exports.model = mongoose.model('Comment', schema);

// // Get Comments
//
// module.exports.getComments = function (callback, limit) {
//     Comment.find(callback).limit(limit);
// };
//
// // Get Comment by Id
//
// module.exports.getCommentById = function (id ,callback) {
//     Comment.findById(id, callback);
// };
//
// // Add comment
//
// module.exports.addComment = function (comment, callback) {
//     Comment.create(comment, callback);
// };
