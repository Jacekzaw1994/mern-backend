const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// User Aim Schema
const schema = exports.schema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    weight: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        enum: ['inprogress', 'done'],
        required: true,
        default: 'inprogress'
    },
    bodyFat: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now,
        index: true
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

exports.model = mongoose.model('UserAim', schema);

// var UserAim = module.exports = mongoose.model('UserAim', userAimSchema);
//
//
// // Get Comments
//
// module.exports.getUserAim = function (callback, limit) {
//     UserAim.find(callback).limit(limit);
// };
//
// // Get Comment by Id
//
// module.exports.getUserAimById = function (id, callback) {
//     UserAim.findById(id, callback);
// };
//
// // Add comment
//
// module.exports.addUserAim = function (user_aim, callback) {
//     UserAim.create(user_aim, callback);
// };