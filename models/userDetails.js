const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// User Details Schema
const schema = exports.schema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    weight: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    },
    body_fat: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now,
        index: true
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

exports.model = mongoose.model('UserDetails', schema);

// var UserDetails = module.exports = mongoose.model('userDetails', userDetailsSchema);
//
//
// // Get User details
//
// module.exports.getUserDetails = function (callback, limit) {
//     UserDetails.find(callback).limit(limit);
// };
//
// // Get Relation by ID
//
// module.exports.getUserDetailById = function (id, callback) {
//     UserDetails.findById(id, callback);
// };
//
// // // Add Relation
//
// module.exports.addUserDetails = function (user_details, callback) {
//     UserDetails.create(user_details, callback);
// };