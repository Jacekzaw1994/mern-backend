const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const createId = require('../utils/createId');

// Post Schema
const schema = exports.schema = new Schema({
    _id: {
        type: String,
        required: true,
        index: true,
        unique: true,
        set: v => `${createId(v)},${Date.now()}`
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    isPublic: {
        type: Boolean,
        default: false
    },
    image: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    publishedAt: {
        type: Date,
        default: Date.now,
        index: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

exports.model = mongoose.model('Post', schema);

// // Get Posts
//
// module.exports.getPosts = function (callback, limit) {
//     Post.find(callback).limit(limit);
// };
//
// // Get Post
//
// module.exports.getPostById = function (id, callback) {
//     Post.findById(id, callback);
// };
//
// // Add Post
//
// module.exports.addPost = function (post, callback) {
//     Post.create(post, callback);
// };
//
// // Update Post
//
// module.exports.updatePost = function (id, post, options, callback) {
//     var query = {_id: id};
//     var update = {
//         title: post.title,
//         description: post.description,
//         date: post.date,
//         image_url: post.image_url,
//         comments: post.comments,
//         likes: post.likes
//     };
//     Post.findOneAndUpdate(query, update, options, callback);
// };
//
// // Delete Post
//
// module.exports.removePost = function (id, callback) {
//     var query = {_id: id};
//     Post.remove(query, callback);
// };