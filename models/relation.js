const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Relation Schema
const schema = exports.schema = new Schema({
    receiver: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    recipient: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    status: {
        type: String,
        enum: ['accepted','waiting', 'rejected'],
        required: true,
        default: 'waiting'
    },
    createdAt: {
        type: Date,
        default: Date.now,
        index: true
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

exports.model = mongoose.model('Relation', schema);

// var Relation = module.exports = mongoose.model('relation', relationSchema);
//
// // Get Relations
//
// module.exports.getRelations = function (callback, limit) {
//     Relation.find(callback).limit(limit);
// };
//
// // Get Relation by ID
//
// module.exports.getRelationById = function (id, callback) {
//     Relation.findById(id, callback);
// };
//
// // Add Relation
//
// module.exports.addRelation = function (relation, callback) {
//     Relation.create(relation, callback);
// };
//
// // Update Relation
//
// module.exports.updateRelation = function (id, relation, options, callback) {
//     var query = {_id: id};
//     var update = {
//         friend_id: relation.friend_id,
//         status: relation.status
//     };
//     Relation.findOneAndUpdate(query, update, options, callback);
// };