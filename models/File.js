const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = exports.schema = new Schema({
    mimeType: {type: String, required: true},
    path: {type: String, required: true},
    size: {type: Number, required: true},
    owner: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    createdAt: {type: Date, default: Date.now, index: true},
    updatedAt: {type: Date, default: Date.now}
});

exports.model = mongoose.model('File', schema);