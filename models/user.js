const {hash} = require('../utils/hash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

function validateEmail(str) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(str);
}

// User Schema
const schema = exports.schema = new Schema({
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        index: true,
        unique: true,
        validate: {
            validator: validateEmail, message: 'Email has a wrong format'
        }
    },
    password: {
        type: String,
        required: true,
        select: false,
        set: v => hash(v)
    },
    birth: {
        type: Date,
        required: true
    },
    gender: {
        type: String,
        enum: ['Male', 'Female'],
        required: true
    }
});

exports.model = mongoose.model('User', schema);






// // Get Users
//
// module.exports.getUsers = function (callback, limit) {
//     User.find(callback).limit(limit);
// };
//
// // Get User by Id
//
// module.exports.getUserById = function (id ,callback) {
//     User.findById(id, callback);
// };
//
// // Add User
//
// module.exports.addUser = function (user, callback) {
//     User.create(user, callback);
// };
//
// // Update User
//
// module.exports.updateUser = function (id, user, options, callback) {
//     var query = {_id: id};
//     var update = {
//         name: user.name,
//         surname: user.surname,
//         email: user.email,
//         password: user.password,
//         gender: user.gender,
//         birth: user.birth,
//         salt: user.salt,
//         user_details: user.user_details,
//         user_aim: user.user_aim,
//         relations: user.relations,
//         posts: user.posts
//     };
//     User.findOneAndUpdate(query, update, options, callback);
// };
//
// // Delete User
//
// module.exports.removeUser = function (user, callback) {
//     var query = {_id: id};
//     User.remove(query, callback);
// };