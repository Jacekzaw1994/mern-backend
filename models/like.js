const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Like Schema
const schema = mongoose.schema = new Schema({
    type: {type: String, enum: ['Like', 'Dislike'], required: true},
    post: {type: String, ref: 'Post', required: true},
    author: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    createdAt: {type: Date, default: Date.now, index: true},
    updatedAt: {type: Date, default: Date.now}
});

exports.model = mongoose.model('Like', schema);