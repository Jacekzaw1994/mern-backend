module.exports = (props) => ({
    User: require('./User').model,
    Post: require('./Post').model,
    Comment: require('./Comment').model,
    Like: require('./Like').model,
    File: require('./File').model,
    Relation: require('./Relation').model,
    UserAim: require('./UserAim').model,
    UserDetails: require('./UserDetails').model,
    Messages: require('./Messages').model
});