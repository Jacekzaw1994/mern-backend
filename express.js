const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes');
const models = require('./models');
const logger = require('./utils/logger');
const error = require('./utils/error');
const auth = require('./utils/auth');
const errorHandler = require('./handlers/error');
const frontURL = require('./config/frontURL');

module.exports = (props = {}) => {
    const app = express();

    // Register middlewares
    app.use(logger());
    app.use(bodyParser.json());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(cors({
        origin: ['http://localhost:3000', frontURL]
    }));

    // Register models
    props.models = models(props);

    // Initialize passport
    app.use(auth.initialize(props));

    // Register API routes
    app.use('/', routes(props));

    // Catch 404 error
    app.use((req, res, next) => {
        next(error(404, 'Not Found'));
    });

    // Register error handler middleware
    app.use(errorHandler);

    return app;
};

