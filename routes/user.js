const isString = require('lodash').isString;
const error = require('../utils/error');

function userExists(model, email = '') {
    return model.find({
        username: `${email}`.toLowerCase()
    }).then(users => users.length > 0);
}


module.exports = ({models: {User}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        User.findById(id)
            .then(user => {
                if (user) {
                    res.json(user);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchCurrent(req, res, next) {
        return res.json(req.user);
    },

    fetchMany(req, res, next ) {
        User.find()
            .sort({email: 1})
            .then(users => res.json(users))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {name, surname, email, password, birth, gender} = req.body;
        userExists(User, email)
            .then(exists => {
                if (!exists) {
                    const user = new User({name, surname, email, password, birth, gender});
                    return user.save();
                } else {
                    throw error(400, 'Email or username is already used');
                }
            })
            .then(user => res.json({ _id: user._id }))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {password} = req.body;
        User.findById(id)
            .then(user => {
                if (user) {
                    if (isString(password)) {
                        user.password = password;
                    }
                    user.updatedAt = Date.now();
                    return user.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res,json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        User.findByIdAndRemove(id)
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    }
});