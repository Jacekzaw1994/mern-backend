const isString = require('lodash').isString;
const isBoolean = require('lodash').isBoolean;
const query = require('../utils/query');
const error = require('../utils/error');

function getConditions({isPublic, author, from, to}) {
    const conditions = {};
    if (isString(isPublic)) {
        conditions.isPublic = (isPublic === 'true' || isPublic === '1');
    }
    if (isString(author)) {
        conditions.author = author;
    }
    if (isString(from)) {
        if (!conditions.publishedAt) {
            conditions.publishedAt = {};
        }
        conditions.publishedAt['$gte'] = from; // inclusive
    }
    if (isString(to)) {
        if (!conditions.publishedAt) {
            conditions.publishedAt = {};
        }
        conditions.publishedAt['$lt'] = to; // exclusive
    }
    return conditions;
}

module.exports = ({models: {Post}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        Post.findById(id)
            .populate('image')
            .populate({
                path: 'author'
            })
            .then(post => {
                if (post) {
                    res.json(post);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        Post.find(getConditions(req.query))
            .select('-content')
            .populate('image')
            .populate({
                path: 'author'
            })
            .sort({publishedAt: -1})
            .skip(query.getSkip(req.query))
            .limit(query.getLimit(req.query))
            .then(posts => res.json(posts))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            title,
            content,
            isPublic,
            image,
            publishedAt
        } = req.body;
        const  post = new Post({
            _id: title,
            author: req.user._id,
            title,
            content,
            isPublic,
            image,
            publishedAt
        });
        post.save()
            .then(post => res.json({ _id: post._id}))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {
            title,
            content,
            isPublic,
            image,
            publishedAt
        } = req.body;
        Post.findById(id)
            .then(post => {
                if (post) {
                    if (isString(title)) {
                        post.title = title;
                    }
                    if (isString(content)) {
                        post.content = content;
                    }
                    if (isBoolean(isPublic)) {
                        post.isPublic = isPublic;
                    }
                    if (isString(image)) {
                        post.image = image;
                    }
                    if (publishedAt) {
                        post.publishedAt = publishedAt
                    }
                    post.updatedAt = Date.now();
                    return post.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        Post.findByIdAndRemove(id)
            .then(() =>res.json({ success: true }))
            .catch(err => next(err));
    }
});