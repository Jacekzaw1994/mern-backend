const isString = require('lodash').isString;
const error = require('../utils/error');

function getConditions({ author, friend, message}) {
    const conditions = {};
    if (isString(author)) {
        conditions.author = author;
    }
    if (isString(friend)) {
        conditions.friend = friend;
    }
    if (isString(message)) {
        conditions.message = message;
    }
    return conditions;
}

module.exports = ({models: {Messages}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        Messages.findById(id)
            .populate('author')
            .then(messages => {
                if (messages) {
                    res.json(messages);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        Messages.find(getConditions(req.query))
            .then(messages => res.json(messages))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            friend,
            message
        } = req.body;
        const  messages = new Messages({
            friend,
            message,
            author: req.user._id
        });
        messages.save()
            .then(messages => res.json({ _id: messages._id}))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {
            message
        } = req.body;
        Messages.findById(id)
            .then(messages => {
                if (messages) {
                    if (isString(message)) {
                        messages.message = message;
                    }
                    messages.updatedAt = Date.now();
                    return messages.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        Messages.findByIdAndRemove(id)
            .then(() =>res.json({ success: true }))
            .catch(err => next(err));
    }
});