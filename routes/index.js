const express = require('express');
const {authorize} = require('../utils/auth');

module.exports = (props) => {
    const router = express.Router();
    const user = require('./user')(props);
    const token = require('./token')(props);
    const post = require('./post')(props);
    const comment = require('./comment')(props);
    const like = require('./like')(props);
    const relation = require('./relation')(props);
    const useraim = require('./useraim')(props);
    const userdetails = require('./userdetails')(props);
    const messages = require('./messages')(props);
    const file = require('./file')(props);

    router.get('/user', user.fetchMany);
    router.get('/user/current', user.fetchCurrent);
    router.get('/user/:id', user.fetch);
    router.put('/user/:id', authorize( 'params.id'), user.update);
    router.delete('/user/:id', authorize('params.id'), user.delete);
    router.post('/user', user.create);

    router.post('/token', token.create);

    router.get('/post', post.fetchMany);
    router.get('/post/:id', post.fetch);
    router.put('/post/:id', post.update);
    router.delete('/post/:id', post.delete);
    router.post('/post', post.create);

    router.get('/comment', comment.fetchMany);
    router.get('/comment/:id', comment.fetch);
    router.put('/comment/:id', comment.update);
    router.delete('/comment/:id', comment.delete);
    router.post('/comment', comment.create);

    router.get('/like', like.fetchMany);
    router.get('/like/:id', like.fetch);
    router.put('/like/:id', like.update);
    router.delete('/like/:id', like.delete);
    router.post('/like', like.create);

    router.get('/relation', relation.fetchMany);
    router.get('/relation/:id', relation.fetch);
    router.put('/relation/:id', relation.update);
    router.delete('/relation/:id', relation.delete);
    router.post('/relation', relation.create);

    router.get('/useraim', useraim.fetchMany);
    router.get('/useraim/:id', useraim.fetch);
    router.put('/useraim/:id', useraim.update);
    router.delete('/useraim/:id', useraim.delete);
    router.post('/useraim', useraim.create);

    router.get('/userdetails', userdetails.fetchMany);
    router.get('/userdetails/:id', userdetails.fetch);
    router.put('/userdetails/:id', userdetails.update);
    router.delete('/userdetails/:id', userdetails.delete);
    router.post('/userdetails', userdetails.create);

    router.get('/messages', messages.fetchMany);
    router.get('/messages/:id', messages.fetch);
    router.put('/messages/:id', messages.update);
    router.delete('/messages/:id', messages.delete);
    router.post('/messages', messages.create);

    router.get('/file', file.fetchMany);
    router.get('/file/:id', file.fetch);
    router.delete('/file/:id', file.delete);
    router.post('/file', file.create);

    return router;
};