const isString = require('lodash').isString;
const query = require('../utils/query');
const error = require('../utils/error');

function getConditions({ receiver, recipient}) {
    const conditions = {};
    if (isString(receiver)) {
        conditions.receiver = receiver;
    }
    if (isString(recipient)) {
        conditions.recipient = recipient;
    }
    return conditions;
}

module.exports = ({models: {Relation}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        Relation.findById(id)
            .populate('recipient')
            .then(relation => {
                if (relation) {
                    res.json(relation);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        Relation.find(getConditions(req.query))
            .then(relations => res.json(relations))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            receiver,
            status
        } = req.body;
        const  relation = new Relation({
            receiver,
            recipient: req.user._id,
            status
        });
        relation.save()
            .then(relation => res.json({ _id: relation._id}))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {
            status
        } = req.body;
        Relation.findById(id)
            .then(relation => {
                if (relation) {
                    if (isString(status)) {
                        relation.status = status;
                    }
                    relation.updatedAt = Date.now();
                    return relation.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        Relation.findByIdAndRemove(id)
            .then(() =>res.json({ success: true }))
            .catch(err => next(err));
    }
});