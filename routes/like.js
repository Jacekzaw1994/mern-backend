const isString = require('lodash').isString;
const query = require('../utils/query');
const error = require('../utils/error');

function getConditions({author, post}) {
    const conditions = {};
    if (isString(author)) {
        conditions.author = author;
    }
    if (isString(post)) {
        conditions.post = post;
    }
    return conditions;
}

module.exports = ({models: {Like}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        Like.findById(id)
            .populate('post', 'title')
            .populate({
                path: 'author'
            })
            .then(like => {
                if (like) {
                    res.json(like);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        Like.find(getConditions(req.query))
            .populate('post', 'title')
            .populate({
                path: 'author'
            })
            .sort({createdAt: -1})
            .skip(query.getSkip(req.query))
            .limit(query.getLimit(req.query))
            .then(likes => res.json(likes))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            type,
            post,
        } = req.body;
        const like = new Like({
            author: req.user._id,
            type,
            post
        });
        like.save()
            .then(like => res.json({ _id: like._id }))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {type} = req.body;
        Like.findById(id)
            .populate('author')
            .then(like => {
                if (like) {
                    const {_id: userId} = req.user;
                    if (`${like.author._id}` !== `${userId}`) {
                        throw error(401, 'You are not author');
                    }
                    if (isString(type)) {
                        like.type = type;
                    }
                    like.updatedAt = Date.now();
                    return like.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        Like.findById(id)
            .populate('author')
            .then(like => {
                if (like) {
                    const {_id: userId} = req.user;
                    if (`${like.author._id}` !== `${userId}`) {
                        throw error(401, 'You are not author');
                    } else {
                        return like.remove();
                    }
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },


});