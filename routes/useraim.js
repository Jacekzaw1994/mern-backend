const isString = require('lodash').isString;
const isNumber = require('lodash').isNumber;
const error = require('../utils/error');

function getConditions({ author, weight, height, bodyfat, status}) {
    const conditions = {};
    if (isString(author)) {
        conditions.author = author;
    }
    if (isNumber(weight)) {
        conditions.weight = weight;
    }
    if (isNumber(height)) {
        conditions.height = height;
    }
    if (isNumber(bodyfat)) {
        conditions.bodyfat = bodyfat;
    }
    if (isString(status)) {
        conditions.status = status;
    }
    return conditions;
}

module.exports = ({models: {UserAim}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        UserAim.findById(id)
            .populate({
                path: 'author'
            })
            .then(useraim => {
                if (useraim) {
                    res.json(useraim);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        UserAim.find(getConditions(req.query))
            .then(relations => res.json(relations))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            weight,
            height,
            bodyfat
        } = req.body;
        const useraim = new UserAim({
            author: req.user._id,
            weight,
            height,
            bodyfat
        });
        useraim.save()
            .then(useraim => res.json({ _id: useraim._id }))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {
            weight,
            height,
            bodyfat
        } = req.body;
        UserAim.findById(id)
            .then(useraim => {
                if (useraim) {
                    if (isNumber(weight)) {
                        conditions.weight = weight;
                    }
                    if (isNumber(height)) {
                        conditions.height = height;
                    }
                    if (isNumber(bodyfat)) {
                        conditions.bodyfat = bodyfat;
                    }
                    if (isString(status)) {
                        conditions.status = status;
                    }
                    useraim.updatedAt = Date.now();
                    return useraim.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        UserAim.findByIdAndRemove(id)
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    }

});