const error = require('../utils/error');
const auth = require('../utils/auth');
const hash = require('../utils/hash');

module.exports = ({models: {User}}) => ({

    create(req, res, next) {
        const {email, password} = req.body;
        User.findOne({ email: `${email}`.toLowerCase() })
            .select(['+password'])
            .then(user => {
                if (user && hash.compare(`${password}`, user.password)) {
                    return auth.encodeToken(user._id);
                } else {
                    throw error(401, 'Wrong email or password');
                }
            })
            .then(token => res.json({ token }))
            .catch(err => next(err));
    }

});