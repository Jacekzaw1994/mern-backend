const isString = require('lodash').isString;
const error = require('../utils/error');
const query = require('../utils/query');
const {upload, remove} = require('../utils/multer');

function getConditions({mimeTypes, owner, from, to}) {
    const conditions = {};
    if (isString(mimeTypes)) {
        mimeTypes = mimeTypes.split(';');
        conditions.mimeType = {'$in': mimeTypes};
    }
    if (isString(owner)) {
        conditions.owner = owner;
    }
    if (isString(from)) {
        if (!conditions.createdAt) {
            conditions.createdAt = {};
        }
        conditions.createdAt['$gte'] = from; // inclusive
    }
    if (isString(to)) {
        if (!conditions.createdAt) {
            conditions.createdAt = {};
        }
        conditions.createdAt['$lt'] = to; // exclusive
    }
    return conditions;
}

module.exports = ({models: {File}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        File.findById(id)
            .populate({
                path: 'owner'
            })
            .then(file => {
                if (file) {
                    res.json(file);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        File.find(getConditions(req.query))
            .populate({
                path: 'owner'
            })
            .sort({createdAt: -1})
            .skip(query.getSkip(req.query))
            .limit(query.getLimit(req.query))
            .then(files => res.json(files))
            .catch(err => next(err));
    },

    create(req, res, next) {
        upload.single('file')(req, res, (err) => {
            if (err) return next(err);
            if (!req.file) return next(error(400, 'File is not specified'));

            const {filename, size, mimetype: mimeType} = req.file;
            const path = `uploads/${filename}`;
            const file = new File({mimeType, path, size, owner: req.user._id});

            return file.save()
                .then(file => res.json({ _id: file._id, path }))
                .catch(err => remove(path).then(() => { throw err; }))
                .catch(err => next(err));
        });
    },

    delete(req, res, next) {
        let path = null;
        const {id} = req.params;
        File.findById(id)
            .populate('owner')
            .then(file => {
                if (file) {
                    const {_id: userId} = req.user;
                    if (`${file.owner._id}` !== `${userId}`) {
                        throw error(401, 'You are not owner');
                    } else {
                        path = file.path;
                        return file.remove();
                    }
                }
            })
            .then(() => path ? remove(path).catch(err => {}) : null)
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    }

});