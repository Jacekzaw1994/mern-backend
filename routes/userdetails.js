const isString = require('lodash').isString;
const isNumber = require('lodash').isNumber;
const error = require('../utils/error');

function getConditions({ author, weight, height, bodyfat}) {
    const conditions = {};
    if (isString(author)) {
        conditions.author = author;
    }
    if (isNumber(weight)) {
        conditions.weight = weight;
    }
    if (isNumber(height)) {
        conditions.height = height;
    }
    if (isNumber(bodyfat)) {
        conditions.bodyfat = bodyfat;
    }
    return conditions;
}

module.exports = ({models: {UserDetails}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        UserDetails.findById(id)
            .populate({
                path: 'author'
            })
            .then(userdetails => {
                if (userdetails) {
                    res.json(userdetails);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        UserDetails.find(getConditions(req.query))
            .then(relations => res.json(relations))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            weight,
            height,
            bodyfat
        } = req.body;
        const userdetails = new UserDetails({
            author: req.user._id,
            weight,
            height,
            bodyfat
        });
        userdetails.save()
            .then(userdetails => res.json({ _id: userdetails._id }))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {
            weight,
            height,
            bodyfat
        } = req.body;
        UserDetails.findById(id)
            .then(userdetails => {
                if (userdetails) {
                    if (isNumber(weight)) {
                        conditions.weight = weight;
                    }
                    if (isNumber(height)) {
                        conditions.height = height;
                    }
                    if (isNumber(bodyfat)) {
                        conditions.bodyfat = bodyfat;
                    }
                    userdetails.updatedAt = Date.now();
                    return userdetails.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        UserDetails.findByIdAndRemove(id)
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    }

});