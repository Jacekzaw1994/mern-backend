const isString = require('lodash').isString;
const query = require('../utils/query');
const error = require('../utils/error');

function getConditions({author, post}) {
    const conditions = {};
    if (isString(author)) {
        conditions.author = author;
    }
    if (isString(post)) {
        conditions.post = post;
    }
    return conditions;
}

module.exports = ({models: {Comment}}) => ({

    fetch(req, res, next) {
        const {id} = req.params;
        Comment.findById(id)
            .populate('post', 'title')
            .populate({
                path: 'author'
            })
            .then(comment => {
                if (comment) {
                    res.json(comment);
                } else {
                    throw error(404);
                }
            })
            .catch(err => next(err));
    },

    fetchMany(req, res, next) {
        Comment.find(getConditions(req.query))
            .populate('post', 'title')
            .populate({
                path: 'author'
            })
            .sort({createdAt: -1})
            .skip(query.getSkip(req.query))
            .limit(query.getLimit(req.query))
            .then(comments => res.json(comments))
            .catch(err => next(err));
    },

    create(req, res, next) {
        const {
            content,
            post
        } = req.body;
        const comment = new Comment({
            author: req.user._id,
            content,
            post
        });
        comment.save()
            .then(comment => res.json({ _id: comment._id }))
            .catch(err => next(err));
    },

    update(req, res, next) {
        const {id} = req.params;
        const {content} = req.body;
        Comment.findById(id)
            .populate('author')
            .then(comment => {
                if (comment) {
                    const {_id: userId} = req.user;
                    if (isString(content)) {
                        comment.content = content;
                    }
                    comment.updatedAt = Date.now();
                    return comment.save();
                } else {
                    throw error(404);
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    },

    delete(req, res, next) {
        const {id} = req.params;
        Comment.findById(id)
            .populate('author')
            .then(comment => {
                if (comment) {
                    const {_id: userId} = req.user;
                    if (`${comment.author._id}` !== `${userId}`) {
                        throw error(401, 'You are not author');
                    } else {
                        return comment.remove();
                    }
                }
            })
            .then(() => res.json({ success: true }))
            .catch(err => next(err));
    }
});